import React from 'react';
import {StyleSheet} from 'react-native';
import {HomeScreen} from '../screens/HomeScreen';
import {MainStackParamList, TabStackScreenProps} from './Navigation';
import {
  BottomTabBarOptions,
  createBottomTabNavigator,
} from '@react-navigation/bottom-tabs';
import SettingScreen from '../screens/SettingScreen';

const Tab = createBottomTabNavigator<MainStackParamList>();
const styles = StyleSheet.create({
  tabBarOption: {
    alignItems: 'stretch',
  },
});
const tabBarOptions: BottomTabBarOptions = {
  activeTintColor: 'red',
  showLabel: true,
  style: styles.tabBarOption,
};

const MainTab: React.FC<TabStackScreenProps<'MainTab'>> = props => {
  console.log(props);
  return (
    <Tab.Navigator initialRouteName="Home" lazy tabBarOptions={tabBarOptions}>
      <Tab.Screen name="Home" component={HomeScreen} />
      <Tab.Screen name="Setting" component={SettingScreen} />
    </Tab.Navigator>
  );
};

export default MainTab;
