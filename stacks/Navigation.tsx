import React, {useContext} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {AuthUserContext} from '../contexts/AuthUserProvider';

import {StackNavigationProp, StackScreenProps} from '@react-navigation/stack';
import LoginStack from './LoginStack';
import MainStack from './MainStack';
import {
  BottomTabNavigationProp,
  BottomTabScreenProps,
} from '@react-navigation/bottom-tabs';

type HomeProps = {
  userId: number;
};
type DetailsProps = {
  id: number;
};

export type TabStackParamList = {
  MainTab: undefined;
};

export type T = keyof TabStackParamList;
export type TabStackScreenProps<RouteName extends T> = BottomTabScreenProps<
  TabStackParamList,
  RouteName
>;

export type TabStackNavigation = BottomTabNavigationProp<TabStackParamList>;

export type AuthStackParamList = {
  Login: undefined;
};

export type MainStackParamList = {
  Home: HomeProps | undefined;
  Setting: undefined;
  Detail: DetailsProps | undefined;
  MovieScreen: undefined;
  PopularMovieScreen: undefined;
};

export type MainStackNavigation = StackNavigationProp<MainStackParamList>;
export type S = keyof MainStackParamList;
export type MainStackScreenProps<RouteName extends S> = StackScreenProps<
  MainStackParamList,
  RouteName
>;

export const AppStack = () => {
  const auth = useContext(AuthUserContext);
  return (
    <NavigationContainer>
      {auth.isAuth ? <MainStack /> : <LoginStack />}
    </NavigationContainer>
  );
};
