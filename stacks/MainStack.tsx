import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import {DetailsScreen} from '../screens/DetailScreen';
import MovieScreen from '../screens/MovieScreen';
import PopularMovieScreen from '../screens/PopularMovieScreen';
import MainTab from './MainTab';
import {MainStackParamList, TabStackParamList} from './Navigation';

const Stack = createStackNavigator<TabStackParamList & MainStackParamList>();
const MainStack = () => {
  return (
    <Stack.Navigator initialRouteName="MainTab">
      <Stack.Screen name="MainTab" component={MainTab} />
      <Stack.Screen name="Detail" component={DetailsScreen} />
      <Stack.Screen name="MovieScreen" component={MovieScreen} />
      <Stack.Screen name="PopularMovieScreen" component={PopularMovieScreen} />
    </Stack.Navigator>
  );
};

export default MainStack;
