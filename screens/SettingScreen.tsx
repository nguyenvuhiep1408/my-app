import React, {useContext, useMemo} from 'react';

import {View, Text, ViewStyle} from 'react-native';
import MyButton, {styles} from '../components/Button';
import {CounterContext} from '../contexts/CouterProvider';
import useTheme from '../hooks/useTheme';
import {MainStackScreenProps} from '../stacks/Navigation';

const SettingScreen: React.FC<MainStackScreenProps<'Setting'>> = ({
  navigation,
}) => {
  const counterContext = useContext(CounterContext);
  const {currentTheme} = useTheme();
  const backgroundStyle: ViewStyle = useMemo(
    () => ({
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: currentTheme?.primaryColor,
    }),
    [currentTheme],
  );
  const buttonBackgroundColor = useMemo(
    () => ({
      backgroundColor: currentTheme?.buttonColor,
    }),
    [currentTheme],
  );
  const textColor = useMemo(
    () => ({
      color: currentTheme?.textColor,
    }),
    [currentTheme],
  );

  return (
    <View style={backgroundStyle}>
      <Text style={textColor}>Setting Screen</Text>
      <MyButton
        style={[styles.button, buttonBackgroundColor]}
        textStyle={textColor}
        buttonText="Movie Screen"
        onPress={() => {
          navigation.navigate('MovieScreen');
        }}
      />
      
      <Text style={textColor}>{counterContext?.counter}</Text>
      <MyButton
        style={[styles.button, buttonBackgroundColor]}
        textStyle={textColor}
        buttonText="Increment"
        onPress={counterContext?.incrementCounter}
      />
      <MyButton
        style={[styles.button, buttonBackgroundColor]}
        textStyle={textColor}
        buttonText="Decrement"
        onPress={counterContext?.decrementCounter}
      />
      <MyButton
        style={[styles.button, buttonBackgroundColor]}
        textStyle={textColor}
        buttonText="Reset"
        onPress={counterContext?.resetCounter}
      />
    </View>
  );
};

export default SettingScreen;
