import React, {useContext, useMemo} from 'react';
import {Text, View, ViewStyle} from 'react-native';
import MyButton, {styles} from '../components/Button';
import {CounterContext} from '../contexts/CouterProvider';
import {ThemeContext} from '../contexts/ThemeProvider';
import useTheme from '../hooks/useTheme';
import {MainStackScreenProps} from '../stacks/Navigation';

export const HomeScreen: React.FC<MainStackScreenProps<'Home'>> = ({
  navigation,
  route,
}) => {
  const counterContext = useContext(CounterContext);
  const themeContext = useContext(ThemeContext);
  const {setDefaultTheme, setBlackTheme} = useTheme();
  const backgroundStyle: ViewStyle = useMemo(
    () => ({
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: themeContext?.theme?.primaryColor,
    }),
    [themeContext],
  );
  const buttonBackgroundColor = useMemo(
    () => ({
      backgroundColor: themeContext?.theme.buttonColor,
    }),
    [themeContext],
  );

  const textColor = useMemo(
    () => ({
      color: themeContext?.theme.textColor,
    }),
    [themeContext],
  );
  return (
    <View style={backgroundStyle}>
      <Text style={textColor}>Counter value = {counterContext?.counter}</Text>
      <Text style={textColor}>Home Screen</Text>
      {route?.params && (
        <Text style={textColor}>
          With received params : {route?.params?.userId}
        </Text>
      )}
      <MyButton
        style={[styles.button, buttonBackgroundColor]}
        textStyle={textColor}
        buttonText="Detail Screen"
        onPress={() => {
          navigation.navigate('Detail', {
            id: Math.floor(Math.random() * 100),
          });
        }}
      />
      <MyButton
        style={[styles.button, buttonBackgroundColor]}
        textStyle={textColor}
        buttonText="Change Black Theme"
        onPress={setBlackTheme}
      />
      <MyButton
        style={[styles.button, buttonBackgroundColor]}
        textStyle={textColor}
        buttonText="Change Default Theme"
        onPress={setDefaultTheme}
      />
    </View>
  );
};
