import {useNavigation} from '@react-navigation/core';
import React, {useContext, useMemo} from 'react';
import {Text, View, ViewStyle} from 'react-native';
import MyButton, {styles} from '../components/Button';
import {ThemeContext} from '../contexts/ThemeProvider';
import {MainStackNavigation, MainStackScreenProps} from '../stacks/Navigation';

export const DetailsScreen: React.FC<MainStackScreenProps<'Detail'>> = ({
  route,
}) => {
  // const tabStackNavigation = useNavigation<TabStackNavigation>();
  const mainStackNavigation = useNavigation<MainStackNavigation>();
  const themeContext = useContext(ThemeContext);
  const backgroundStyle: ViewStyle = useMemo(
    () => ({
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: themeContext?.theme?.primaryColor,
    }),
    [themeContext],
  );
  const buttonBackgroundColor = useMemo(
    () => ({
      backgroundColor: themeContext?.theme.buttonColor,
    }),
    [themeContext],
  );

  const textColor = useMemo(
    () => ({
      color: themeContext?.theme.textColor,
    }),
    [themeContext],
  );

  return (
    <View style={backgroundStyle}>
      <Text style={textColor}>
        Details Screen with params: {route?.params?.id}
      </Text>
      <MyButton
        style={[styles.button, buttonBackgroundColor]}
        textStyle={textColor}
        buttonText="Go other detail screen"
        onPress={() => {
          mainStackNavigation.push('Detail', {
            id: Math.floor(Math.random() * 100),
          });
        }}
      />
      <MyButton
        style={[styles.button, buttonBackgroundColor]}
        textStyle={textColor}
        buttonText="Go Back Home screen with put back param"
        onPress={() => {
          mainStackNavigation.navigate({
            name: 'Home',
            params: {userId: Math.floor(Math.random() * 100)},
          });
        }}
      />
      <MyButton
        style={[styles.button, buttonBackgroundColor]}
        textStyle={textColor}
        buttonText="Go Back Top screen"
        onPress={() => {
          mainStackNavigation.popToTop();
        }}
      />
      <MyButton
        style={[styles.button, buttonBackgroundColor]}
        textStyle={textColor}
        buttonText="Go Back"
        onPress={() => {
          mainStackNavigation.goBack();
        }}
      />
    </View>
  );
};
