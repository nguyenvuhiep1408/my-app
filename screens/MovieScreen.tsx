import React, { useEffect, useState, useMemo } from 'react';
import { ActivityIndicator, FlatList, Text, View } from 'react-native';
import MyButton, { styles } from '../components/Button';
import { MainStackScreenProps } from '../stacks/Navigation';
import useTheme from '../hooks/useTheme';

const MovieScreen: React.FC<MainStackScreenProps<'Setting'>> = ({ navigation, }) => {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const {currentTheme} = useTheme();
  const buttonBackgroundColor = useMemo(
    () => ({
      backgroundColor: currentTheme?.buttonColor,
      // alignItems: 'center',
    }),
    [currentTheme],
  );
  const textColor = useMemo(
    () => ({
      color: currentTheme?.textColor,
    }),
    [currentTheme],
  );

  useEffect(() => {
    //https://en.wikipedia.org/wiki/Representational_state_transfer
    fetch('https://reactnative.dev/movies.json', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then(response => response.json())
      .then(json => setData(json.movies))
      .catch(error => console.error(error))
      .finally(() => setLoading(false));
  }, []);

  return (
    // eslint-disable-next-line react-native/no-inline-styles
    <View style={{ flex: 1, padding: 24 }}>
      {isLoading ? (
        <ActivityIndicator />
      ) : (
        <FlatList
          data={data}
          keyExtractor={({ id }: any, index) => id + index}
          renderItem={({ item }: any) => (
            <Text>
              {item?.title}, {item.releaseYear}
            </Text>
          )}
        />
      )}
      <MyButton
        style={[styles.button, buttonBackgroundColor]}
        textStyle={textColor}
        buttonText="Popular Movie Screen"
        onPress={() => {
          navigation.navigate('PopularMovieScreen');
        }}
      />
    </View>
  );
};
export default MovieScreen;
