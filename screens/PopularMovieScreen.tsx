import React, { useEffect, useState } from 'react';
import { ActivityIndicator, FlatList, Image, Text, View, StyleSheet } from 'react-native';

const PopularMovieScreen = () => {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const baseImgUrl = "https://image.tmdb.org/t/p"
  const size = "w500"
  const styles = StyleSheet.create({
    tinyLogo: {
      width: 50,
      height: 50,
    }
  });
  
  useEffect(() => {
    //https://en.wikipedia.org/wiki/Representational_state_transfer
    fetch('https://api.themoviedb.org/3/movie/popular?api_key=4cf0964a89b8a36fca020edf7cda3956&language=en-US&page=1', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then(response => response.json())
      .then(json => setData(json.results))
      .catch(error => console.error(error))
      .finally(() => setLoading(false));
  }, []);

  return (
    
    // eslint-disable-next-line react-native/no-inline-styles
    <View style={{ flex: 1, padding: 24 }}>
      {isLoading ? (
        <ActivityIndicator />
      ) : (
        <FlatList
          data={data}
          keyExtractor={({ id }: any, index) => id + index}
          renderItem={({ item }: any) => (
            <View>
              <Image style={styles.tinyLogo} source={{ uri: `${baseImgUrl}/${size}${item.poster_path}` }} />
              {/* <Image source={{ uri: 'https://cellphones.com.vn/sforum/wp-content/uploads/2020/04/LR-29-scaled.jpg' }} /> */}
              <Text>
                {item?.title}
              </Text>
            </View>
          )}
        />
      )}
    </View>
  );
};
export default PopularMovieScreen;
