/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */
 import 'react-native-gesture-handler';
 import React from 'react';
 import {AuthUserProvider} from './contexts/AuthUserProvider';
 import {ThemeProvider} from './contexts/ThemeProvider';
 import {AppStack} from './stacks/Navigation';
 import {CounterProvider} from './contexts/CouterProvider';
 
 const App = () => {
   return (
     <ThemeProvider>
       <CounterProvider>
         <AuthUserProvider>
           <AppStack />
         </AuthUserProvider>
       </CounterProvider>
     </ThemeProvider>
   );
 };
 
 export default App;
 