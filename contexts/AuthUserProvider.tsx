import React, {PropsWithChildren, useMemo, useState} from 'react';

export type Auth = {
  isAuth?: boolean;
  setAuth?: (isAuth: boolean) => void;
};

export const AuthUserContext = React.createContext<Auth>({
  isAuth: false,
});

export const AuthUserProvider: React.FC<PropsWithChildren<React.ReactNode>> = ({
  children,
}) => {
  const [isAuth, setAuth] = useState<boolean>();

  const authValue = useMemo(() => ({isAuth, setAuth}), [isAuth, setAuth]);

  return (
    <AuthUserContext.Provider value={authValue}>
      {children}
    </AuthUserContext.Provider>
  );
};
