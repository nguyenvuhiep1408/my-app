import React, {PropsWithChildren} from 'react';
import {useCounter} from '../hooks/useCounter';

type CounterContextType = {
  counter: number;
  incrementCounter?: () => void;
  decrementCounter?: () => void;
  resetCounter?: () => void;
};

export const CounterContext = React.createContext<CounterContextType>({
  counter: 0,
});

export const CounterProvider: React.FC<PropsWithChildren<React.ReactNode>> = ({
  children,
}) => {
  const counterValue = useCounter();
  return (
    <CounterContext.Provider value={counterValue}>
      {children}
    </CounterContext.Provider>
  );
};
